package com.example.bottomsup.view

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.bottomsup.databinding.ItemCategoryBinding
import com.example.bottomsup.databinding.ItemDrinkBinding
import com.example.bottomsup.model.response.CategoryDrinksDTO

class DrinksAdapter : RecyclerView.Adapter<DrinksAdapter.DrinksViewHolder>() {
    private var drinks = mutableListOf<CategoryDrinksDTO.Drink>()
    var adapterListener: ((String) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DrinksViewHolder {
        val binding = ItemDrinkBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DrinksViewHolder(binding)
    }

    override fun onBindViewHolder(drinksViewHolder: DrinksViewHolder, position: Int) {
        var drink = drinks[position]
        drinksViewHolder.loadDrink(drink)
        drinksViewHolder.setClickListener {
            adapterListener?.invoke(drink.idDrink)
        }

    }

    fun addDrinks(drinks: List<CategoryDrinksDTO.Drink>){
        this.drinks = drinks.toMutableList()
        notifyDataSetChanged()
    }

    fun addItemClickListener(function: (String)-> Unit){
        adapterListener = function
    }

    override fun getItemCount(): Int {
        return drinks.size
    }

    class DrinksViewHolder(
        private val binding: ItemDrinkBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadDrink(drink: CategoryDrinksDTO.Drink) {
            binding.tvDrink.text = drink.strDrink
            binding.ivDrink.load(drink.strDrinkThumb)
        }
        fun setClickListener(clickListener: View.OnClickListener?) {
            binding.ivDrink.setOnClickListener(clickListener)
        }
    }
}
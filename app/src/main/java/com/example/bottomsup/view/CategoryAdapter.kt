package com.example.bottomsup.view

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemCategoryBinding
import com.example.bottomsup.model.response.CategoryDTO

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categories = mutableListOf<CategoryDTO.CategoryItem>()
    private var adapterListener: ((String) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryViewHolder {
        val binding = ItemCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(categoryViewHolder: CategoryViewHolder, position: Int) {
        var category = categories[position]
        categoryViewHolder.loadCategory(category.strCategory)
        categoryViewHolder.setClickListener {
            Log.d("HERE IT IS!!!!", category.strCategory)
            adapterListener?.invoke(category.strCategory)
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategories(categories: List<CategoryDTO.CategoryItem>) {
        this.categories = categories.toMutableList()
        notifyDataSetChanged()
    }

    fun addItemClickListener(function: (String) -> Unit) {
        adapterListener = function
    }


    class CategoryViewHolder(
        private val binding: ItemCategoryBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadCategory(category: String) {
            binding.tvCategory.text = category
        }

        fun setClickListener(clickListener: View.OnClickListener?) {
            binding.tvCategory.setOnClickListener(clickListener)
        }

    }

}